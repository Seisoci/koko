package id.ac.darmajaya.searchbruteforce.Adapter;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import id.ac.darmajaya.searchbruteforce.Models.Algo;
import id.ac.darmajaya.searchbruteforce.R;


public class AlgoAdapter extends RecyclerView.Adapter<AlgoAdapter.MyViewHolder> {

    private List<Algo> moviesList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView singakatan, panjang, awal, akhir;

        public MyViewHolder(View view) {
            super(view);
            singakatan = (TextView) view.findViewById(R.id.singaktantv);
            awal = (TextView) view.findViewById(R.id.awal);
            akhir = (TextView) view.findViewById(R.id.akhir);
            panjang  = (TextView) view.findViewById(R.id.panjanghuruf);
        }
    }


    public AlgoAdapter(List<Algo> moviesList) {
        this.moviesList = moviesList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.algo_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Algo movie = moviesList.get(position);
        holder.singakatan.setText(movie.getSingkatan());
        holder.awal.setText(movie.getStartposisi());
        holder.akhir.setText(movie.getEndpos());
        holder.panjang.setText(movie.getPanjanghuruf());

    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}