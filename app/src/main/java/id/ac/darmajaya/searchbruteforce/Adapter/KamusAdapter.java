package id.ac.darmajaya.searchbruteforce.Adapter;


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import id.ac.darmajaya.searchbruteforce.Models.Kamus;
import id.ac.darmajaya.searchbruteforce.PenjelasanActivity;
import id.ac.darmajaya.searchbruteforce.R;

public class KamusAdapter extends RecyclerView.Adapter<KamusAdapter.ViewHolder> {

    public List<Kamus> data;

    public KamusAdapter(){}
    public void setData(List<Kamus> data){
        this.data=data;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context=parent.getContext();
        LayoutInflater inflater=LayoutInflater.from(context);

        View wordView=inflater.inflate(R.layout.kamus_item,parent,false);

        ViewHolder viewHolder=new ViewHolder(wordView,context);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Kamus Kamus=data.get(position);
        holder.wordText.setText(Kamus.getSingakatan());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public Context context;
        public TextView wordText;
        public ViewHolder(View itemView, final Context context) {
            super(itemView);
            this.context=context;
            wordText=(TextView) itemView.findViewById(R.id.namatokotext);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position=getAdapterPosition();
                    Kamus Kamus=data.get(position);

                    Intent intent=new Intent(context, PenjelasanActivity.class);
                    intent.putExtra("ID",Kamus.getId());
                    intent.putExtra("SINGKATAN",Kamus.getPenjelasan());
                    intent.putExtra("PENJELASAN",Kamus.getSingakatan());
                    intent.putExtra("TEMPAT", Kamus.getTempat());
                    context.startActivity(intent);
                }
            });
        }
    }
}
