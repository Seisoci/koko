package id.ac.darmajaya.searchbruteforce;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class PenjelasanActivity extends AppCompatActivity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.penjelasan_activity);


        String singatakan = getIntent().getStringExtra("SINGKATAN");
        String penjelasan = getIntent().getStringExtra("PENJELASAN");
        String tempat = getIntent().getStringExtra("TEMPAT");

        TextView singkatantv = (TextView) findViewById(R.id.singakatan);
        TextView penjelasantv = (TextView) findViewById(R.id.penjelasan);
        TextView temapattv = (TextView) findViewById(R.id.tempat);

        singkatantv.setText(singatakan);
        penjelasantv.setText(penjelasan);
        temapattv.setText(tempat);

    }
}
