package id.ac.darmajaya.searchbruteforce;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.widget.Button;

import java.util.List;

import id.ac.darmajaya.searchbruteforce.Adapter.KamusAdapter;
import id.ac.darmajaya.searchbruteforce.Database.DatabaseHelper;
import id.ac.darmajaya.searchbruteforce.Models.Kamus;

public class ListKamusActivity extends AppCompatActivity {

    private RecyclerView rvWord;
    private KamusAdapter word_adapter;
    private List<Kamus> dictionaryModelList;
    String text;
    private DatabaseHelper mDBHelper;
    Intent cekk;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rcv_item);

        rvWord=(RecyclerView) findViewById(R.id.rvWord);
        rvWord.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        RecyclerView.ItemDecoration itemDecoration=new DividerItemDecoration(getApplicationContext(),DividerItemDecoration.VERTICAL);
        rvWord.addItemDecoration(itemDecoration);

        mDBHelper=new DatabaseHelper(ListKamusActivity.this);

        dictionaryModelList=mDBHelper.getalldata();

        word_adapter=new KamusAdapter();
        word_adapter.setData(dictionaryModelList);
        rvWord.setAdapter(word_adapter);

        final SearchView searchView=(SearchView) findViewById(R.id.searchView);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String newText) {
                cekk = new Intent(ListKamusActivity.this, AlgoActivity.class);
                cekk.putExtra("data", newText);
                startActivity(cekk);
                searchWord(newText);

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                searchWord(newText);
                return false;
            }
        });


        Button cek = (Button) findViewById(R.id.cek);

        cek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CharSequence searchViewQuery = searchView.getQuery();
                cekk = new Intent(ListKamusActivity.this, AlgoActivity.class);
                cekk.putExtra("data", searchViewQuery);
                startActivity(cekk);
            }
        });
    }



    public void searchWord(String newText) {
        dictionaryModelList.clear();
        dictionaryModelList = mDBHelper.getListWord(newText);

        word_adapter.setData(dictionaryModelList);
        rvWord.setAdapter(word_adapter);
    }



}
