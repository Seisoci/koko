package id.ac.darmajaya.searchbruteforce.Models;

public class Kamus {

    private String id;
    private String singakatan;
    private String penjelasan;
    private String tempat;

    public Kamus(String id, String singakatan, String penjelasan, String tempat) {
        this.id = id;
        this.singakatan = singakatan;
        this.penjelasan = penjelasan;
        this.tempat = tempat;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSingakatan() {
        return singakatan;
    }

    public void setSingakatan(String singakatan) {
        this.singakatan = singakatan;
    }

    public String getPenjelasan() {
        return penjelasan;
    }

    public void setPenjelasan(String penjelasan) {
        this.penjelasan = penjelasan;
    }

    public String getTempat() {
        return tempat;
    }

    public void setTempat(String tempat) {
        this.tempat = tempat;
    }

}
