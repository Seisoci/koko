package id.ac.darmajaya.searchbruteforce;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import id.ac.darmajaya.searchbruteforce.Adapter.AlgoAdapter;
import id.ac.darmajaya.searchbruteforce.Algoritma.BruteForce;
import id.ac.darmajaya.searchbruteforce.Database.DatabaseHelper;
import id.ac.darmajaya.searchbruteforce.Models.Algo;
import id.ac.darmajaya.searchbruteforce.Models.Kamus;

public class AlgoActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private AlgoAdapter mAdapter;
    private DatabaseHelper mDBHelper;
    private List<Kamus> dictionaryModelList;
    String data;
    private List<Algo> movieList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rcva_item);

        mDBHelper=new DatabaseHelper(AlgoActivity.this);
        dictionaryModelList=mDBHelper.getalldata();
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        String text = getIntent().getStringExtra("data").toUpperCase();
        searchWord(text);

    }

    public void searchWord(String newText) {
        dictionaryModelList.clear();
        dictionaryModelList = mDBHelper.getListWord(newText);

        BruteForce obj = new BruteForce();
        for (Kamus p : dictionaryModelList) {
            String text = p.getSingakatan().toUpperCase();
            int position = obj.BruteForce(text, newText);
            int endindex = position;

            if (position == -1) {
                System.out.println("Pattern tidak sama dengan text");
            } else {
                    /*
                    Algo data = new Algo();
                    data.setSingkatan(text);
                    data.setPosisi(position + 1);
                    data.setEndpos(endindex + newText.length());
*/
                //System.out.println(text);
                //System.out.println("======================");
                // System.out.println("Found at position:" + (position + 1));
                // System.out.println("End at the position:" + (endindex + tobematched.length()));
                int a = position +1;
                int b = endindex +newText.length();

                String mulaiposisi = String.valueOf("Posisi Awal "+a);
                String akhirposisi = String.valueOf("Posisi Akhir "+b);
                String panjanghuruf = String.valueOf("Panjang Huruf " +text.length());
                System.out.print(text);
                Algo movie = new Algo(text, panjanghuruf, mulaiposisi, akhirposisi);
                movieList.add(movie);
            }
        }
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        mAdapter = new AlgoAdapter(movieList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
    }



}