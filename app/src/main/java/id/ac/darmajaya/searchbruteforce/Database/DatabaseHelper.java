package id.ac.darmajaya.searchbruteforce.Database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import id.ac.darmajaya.searchbruteforce.Models.Kamus;



public class DatabaseHelper extends SQLiteOpenHelper {

    public static final String DBNAME="kamus.sqlite";
    public static final String DBLOCATION="/data/data/id.ac.darmajaya.searchbruteforce/databases/";
    private Context mContext;
    private SQLiteDatabase mDatabase;


    public boolean copyDatabase(Context context) {
        try {
            InputStream inputStream = context.getAssets().open(DatabaseHelper.DBNAME);
            String outFileName = DatabaseHelper.DBLOCATION + DatabaseHelper.DBNAME;
            OutputStream outputStream = new FileOutputStream(outFileName);
            byte[] buff = new byte[1024];
            int length;
            while ((length = inputStream.read(buff)) > 0) {
                outputStream.write(buff, 0, length);
            }
            outputStream.flush();
            outputStream.close();
            Log.w("Database", "Copy Success");
            return true;

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public DatabaseHelper(Context context){
        super(context,DBNAME,null,1);
        this.mContext=context;
    }
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

    }
    
    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
    
    public void openDatabase(){
        String dbPath=mContext.getDatabasePath(DBNAME).getPath();
        if(mDatabase!=null && mDatabase.isOpen()){
            return;
        }
        mDatabase=SQLiteDatabase.openDatabase(dbPath,null,SQLiteDatabase.OPEN_READWRITE);
    }
    
    public void closeDatabase(){
        if(mDatabase !=null){
            mDatabase.close();
        }
    }

    public List<Kamus> getListWord(String wordSearch){
        Kamus dictionaryModel=null;
        List<Kamus> dictionaryModelList=new ArrayList<>();
        openDatabase();
        String[] args={"%"+wordSearch+"%"};

        Cursor cursor=mDatabase.rawQuery("Select * From tblkamus Where singkatan Like ? order by singkatan asc",args);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()){
            dictionaryModel=new Kamus(cursor.getString(0),cursor.getString(1),cursor.getString(2), cursor.getString(3));
            dictionaryModelList.add(dictionaryModel);
            cursor.moveToNext();
        }
        cursor.close();
        closeDatabase();
        return dictionaryModelList;
    }


    public List<Kamus> getalldata(){
        Kamus dictionaryModel=null;
        List<Kamus> dictionaryModelList=new ArrayList<>();
        openDatabase();

        Cursor cursor=mDatabase.rawQuery("Select * From tblkamus", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()){
            dictionaryModel=new Kamus(cursor.getString(0),cursor.getString(1),cursor.getString(2), cursor.getString(3));
            dictionaryModelList.add(dictionaryModel);
            cursor.moveToNext();
        }
        cursor.close();
        closeDatabase();
        return dictionaryModelList;
    }

}
