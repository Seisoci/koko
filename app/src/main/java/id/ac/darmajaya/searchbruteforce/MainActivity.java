package id.ac.darmajaya.searchbruteforce;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.io.File;


import id.ac.darmajaya.searchbruteforce.Database.DatabaseHelper;

public class MainActivity extends AppCompatActivity {

    private DatabaseHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

            db = new DatabaseHelper(this);
        File database = getApplicationContext().getDatabasePath(DatabaseHelper.DBNAME);
        if (database.exists()) {
            db.getReadableDatabase();
            if (db.copyDatabase(this)){
                //Toast.makeText(getApplicationContext(), "Copy success", Toast.LENGTH_LONG).show();
            } else {
                //Toast.makeText(getApplicationContext(), "Copy failed", Toast.LENGTH_LONG).show();
                return;
            }
        }
        if (!database.exists()) {
            db.getReadableDatabase();
            if (db.copyDatabase(this)){
                //Toast.makeText(getApplicationContext(), "Copy success", Toast.LENGTH_LONG).show();
            } else {
                //Toast.makeText(getApplicationContext(), "Copy failed", Toast.LENGTH_LONG).show();
                return;
            }
        }


        Button btnkamus = (Button) findViewById(R.id.kamusbtn);

        btnkamus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, ListKamusActivity.class));
            }
        });
        Button btntentang = (Button) findViewById(R.id.tentangbtn);

        btntentang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, TentangAplikasi.class));
            }
        });
    }



}
